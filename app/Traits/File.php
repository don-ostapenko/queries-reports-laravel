<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use SimpleXMLElement;

trait File
{

    /**
     * Make unique name for report file.
     *
     * @param $company
     * @param  string  $type
     *
     * @return string
     * @throws \Exception
     */
    public function makeFileName($company, $type = 'csv')
    {
        $date = date('d-m-Y-H.i.s', time());
        $companyName = lcfirst(str_replace(' ', '-', $company['query1'][0]->company_name));
        $uniqueValue = $this->getUniqueValue();
        return sprintf('%s_%s_%s.%s', $companyName, $date, $uniqueValue, $type);
    }

    /**
     * Get unique value.
     *
     * @return string
     * @throws \Exception
     */
    private function getUniqueValue(): string
    {
        return substr(sha1(random_bytes(100)), 0, 15);
    }

    /**
     * Make and save CSV file from array.
     *
     * @param $data
     * @param $fileName
     */
    private function saveCsvFile($data, $fileName)
    {
        $tempFile = fopen('php://memory', 'w');
        foreach ($data as $item) {
            fputcsv($tempFile, $item);
        }
        rewind($tempFile);
        Storage::put($fileName, $tempFile);
    }

    /**
     * Make and save XLSX file.
     *
     * @param $data
     * @param $fileName
     *
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    private function saveXlsxFile($data, $fileName)
    {
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->fromArray($data);
        $writer = new Xlsx($spreadsheet);
        $writer->save($fileName);
    }

    /**
     * Make and save XML file from array.
     *
     * @param $data
     * @param $fileName
     */
    private function saveXmlFile($data, $fileName)
    {
        $xml = new SimpleXMLElement('<report/>');
        array_walk_recursive($data, array ($xml, 'addChild'));
        $xmlString = $xml->asXML();

        $tempFile = fopen('php://memory', 'w');
        fputs($tempFile, $xmlString);
        rewind($tempFile);

        Storage::put($fileName, $tempFile);
    }

    /**
     * Make and save JSON file from array.
     *
     * @param $data
     * @param $fileName
     */
    private function saveJsonFile($data, $fileName)
    {
        $data = json_encode($data);

        $tempFile = fopen('php://memory', 'w');
        fputs($tempFile, $data);
        rewind($tempFile);

        Storage::put($fileName, $tempFile);
    }

}
