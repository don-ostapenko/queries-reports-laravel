<?php

namespace App\Providers;

use App\Services\QueryService;
use App\Services\QueryServiceInterface;
use App\Services\ReportService;
use App\Services\ReportTemplateService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(QueryServiceInterface::class, QueryService::class);
        $this->app->bind(ReportService::class);
        $this->app->bind(ReportTemplateService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
