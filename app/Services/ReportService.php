<?php

namespace App\Services;

use App\Traits\File;

class ReportService
{

    use File;

    const PATH_TO_REPORTS = '/var/www/html/resources/reports/';

    /**
     * @var \App\Services\QueryServiceInterface
     */
    protected $queryService;

    /**
     * @var \App\Services\ReportTemplateService
     */
    protected $reportTemplateService;

    /**
     * @var object
     */
    protected $data;

    /**
     * @var
     */
    protected $sortedData;

    /**
     * FirstController constructor.
     *
     * @param  \App\Services\QueryServiceInterface  $queryService
     * @param  \App\Services\ReportTemplateService  $reportTemplateService
     */
    public function __construct(
        QueryServiceInterface $queryService,
        ReportTemplateService $reportTemplateService
    ) {
        $this->queryService = $queryService;
        $this->reportTemplateService = $reportTemplateService;
    }

    /**
     * Make reports and save them to the storage.
     *
     * @throws \Exception
     */
    public function makeReports()
    {
        $this->getDataForReports()->sortDataByCompany();
        $this->saveCsvReports();
        $this->saveXlsxReports();
        $this->saveXmlReports();
        $this->saveJsonReports();
    }

    /**
     * Get data for reports from DB.
     *
     * @return $this
     */
    private function getDataForReports()
    {
        $response['query1'] = ($this->queryService
            ->getAmountAllSubscribedCustomersByCompany(true))
            ->groupBy('company_name');
        $response['query2'] = ($this->queryService
            ->getAmountDisabledSubscribedCustomersByCompany(true))
            ->groupBy('company_name');
        $response['query3'] = ($this->queryService
            ->getTariffsWithSubscribedCustomersByCompany(true))
            ->groupBy('company_name');
        $response['query4'] = ($this->queryService
            ->getActiveCustomersWithSubscribedTariffs(true))
            ->groupBy('company_name');

        $this->data = $response;
        return $this;
    }

    /**
     * Prepare data for reports.
     *
     * @return $this
     */
    private function sortDataByCompany()
    {
        $i = 1;
        foreach ($this->data as $query) {
            $this->parseQueryResult($query, $i);
            $i++;
        }

        return $this;
    }

    /**
     * Parse query result by company.
     *
     * @param $query
     * @param $i
     */
    private function parseQueryResult($query, $i)
    {
        foreach ($query as $company) {
            $this->sortedData[$company[0]->company_name]['query' . $i] = $company;
        }
    }

    /**
     * Save CSV reports to storage.
     *
     * @param  string  $type
     *
     * @throws \Exception
     */
    public function saveCsvReports($type = 'csv')
    {
        foreach ($this->sortedData as $company) {
            $fileName = $this->makeFileName($company, $type);
            $template = $this->reportTemplateService->prepareCsvTemplate($company);
            $this->saveCsvFile($template, $fileName);
        }
    }

    /**
     * Save XLSX reports to storage.
     *
     * @param  string  $type
     *
     * @throws \Exception
     */
    public function saveXlsxReports($type = 'xlsx')
    {
        foreach ($this->sortedData as $company) {
            $fileName = $this->makeFileName($company, $type);
            $template = $this->reportTemplateService->prepareCsvTemplate($company);
            $this->saveXlsxFile($template, self::PATH_TO_REPORTS . $fileName);
        }
    }

    /**
     * Save XML reports to storage.
     *
     * @param  string  $type
     *
     * @throws \Exception
     */
    public function saveXmlReports($type = 'xml')
    {
        foreach ($this->sortedData as $company) {
            $fileName = $this->makeFileName($company, $type);
            $template = $this->reportTemplateService->prepareXmlTemplate($company);
            $this->saveXmlFile($template, $fileName);
        }
    }

    /**
     * Save Json reports to storage.
     *
     * @param  string  $type
     *
     * @throws \Exception
     */
    public function saveJsonReports($type = 'json')
    {
        foreach ($this->sortedData as $company) {
            $fileName = $this->makeFileName($company, $type);
            $template = $this->reportTemplateService->prepareJsonTemplate($company);
            $this->saveJsonFile($template, $fileName);
        }
    }

}
