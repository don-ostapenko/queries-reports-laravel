<?php

namespace App\Services;

interface QueryServiceInterface
{

    /**
     * Get a list of companies that include the amount of all subscribed customers.
     *
     * @param  bool  $collection
     *
     * @return array|\Illuminate\Support\Collection
     */
    public function getAmountAllSubscribedCustomersByCompany(bool $collection);

    /**
     * Get a list of companies that include the amount of disabled subscribed customers.
     *
     * @param  bool  $collection
     *
     * @return array|\Illuminate\Support\Collection
     */
    public function getAmountDisabledSubscribedCustomersByCompany(bool $collection);

    /**
     * Get a list of tariffs with the amount of subscribed customers by the company.
     *
     * @param  bool  $collection
     *
     * @return array|\Illuminate\Support\Collection
     */
    public function getTariffsWithSubscribedCustomersByCompany(bool $collection);

    /**
     * Get a list of active customers with subscribed tariffs.
     *
     * @param  bool  $collection
     *
     * @return array|\Illuminate\Support\Collection
     */
    public function getActiveCustomersWithSubscribedTariffs(bool $collection);

}
