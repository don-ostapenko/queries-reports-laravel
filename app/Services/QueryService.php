<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class QueryService implements QueryServiceInterface
{

    /**
     * {@inheritDoc}
     */
    public function getAmountAllSubscribedCustomersByCompany(bool $collection = false)
    {
        $queryResult = DB::table('companies')
            ->select(DB::raw('companies.name as company_name, count(companies.name) as subscribed_customers'))
            ->join('tariffs', 'companies.id', '=', 'tariffs.company_id')
            ->join('customer_tariff', 'tariffs.id', '=', 'customer_tariff.tariff_id')
            ->groupBy('companies.name')
            ->orderBy('companies.name')
            ->get()
            ->all();

        if ($collection) {
            $queryResult = collect($queryResult);
        }

        return $queryResult;
    }

    /**
     * {@inheritDoc}
     */
    public function getAmountDisabledSubscribedCustomersByCompany(bool $collection = false)
    {
        $queryResult = DB::table('companies')
            ->select(DB::raw('companies.name as company_name, count(companies.name) as disabled_subscribed_customers'))
            ->join('tariffs', 'companies.id', '=', 'tariffs.company_id')
            ->join('customer_tariff', 'tariffs.id', '=', 'customer_tariff.tariff_id')
            ->where('customer_tariff.status', 0)
            ->groupBy('companies.name')
            ->orderBy('companies.name')
            ->get()
            ->all();

        if ($collection) {
            $queryResult = collect($queryResult);
        }

        return $queryResult;
    }

    /**
     * {@inheritDoc}
     */
    public function getTariffsWithSubscribedCustomersByCompany(bool $collection = false)
    {
        $queryResult = DB::table('companies')
            ->select(DB::raw('companies.name as company_name, tariffs.name as tariff_name, count(tariffs.name) as customers_count'))
            ->join('tariffs', 'companies.id', '=', 'tariffs.company_id')
            ->join('customer_tariff', 'tariffs.id', '=', 'customer_tariff.tariff_id')
            ->where('customer_tariff.status', 1)
            ->groupBy('tariffs.name')
            ->orderBy('tariffs.name')
            ->get()
            ->all();

        if ($collection) {
            $queryResult = collect($queryResult);
        }

        return $queryResult;
    }

    /**
     * {@inheritDoc}
     */
    public function getActiveCustomersWithSubscribedTariffs(bool $collection = false)
    {
        $queryResult = DB::table('tariffs')
            ->select(DB::raw('companies.name as company_name, customers.name as customer_name, tariffs.name as tariff_name'))
            ->join('customer_tariff', 'tariffs.id', '=', 'customer_tariff.tariff_id')
            ->join('companies', 'tariffs.company_id', '=', 'companies.id')
            ->join('customers', 'customer_tariff.customer_id', '=', 'customers.id')
            ->where('customer_tariff.status', 1)
            ->orderBy('companies.name')
            ->get()
            ->all();

        if ($collection) {
            $queryResult = collect($queryResult);
        }

        return $queryResult;
    }

}
