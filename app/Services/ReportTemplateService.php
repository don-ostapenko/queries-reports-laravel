<?php

namespace App\Services;

class ReportTemplateService
{

    /**
     * Prepare CSV template.
     *
     * @param $company
     *
     * @return array
     */
    public function prepareCsvTemplate($company)
    {
        $template = [];
        $template[] = [$company['query1'][0]->company_name, date('d-m-Y H:i:s', time())];
        $template[] = ['', ''];
        $template[] = ['subscribed customers', $company['query1'][0]->subscribed_customers];

        if (isset($company['query2'])) {
            $template[] = ['disabled subscribed customers', $company['query2'][0]->disabled_subscribed_customers];
        }

        if (isset($company['query3'])) {
            $template[] = ['', ''];
            $template[] = ['tariff name', 'customers'];
            foreach ($company['query3'] as $tariff) {
                $template[] = [$tariff->tariff_name, $tariff->customers_count];
            }
        }

        if (isset($company['query4'])) {
            $template[] = ['', ''];
            $template[] = ['enabled customers', 'tariffs'];
            foreach ($company['query4'] as $customer) {
                $template[] = [$customer->customer_name, $customer->tariff_name];
            }
        }

        return $template;
    }

    /**
     * Prepare XML template.
     *
     * @param $company
     *
     * @return array
     */
    public function prepareXmlTemplate($company)
    {
        $template = [
            $company['query1'][0]->company_name => 'company_name',
            date('d-m-Y H:i:s', time()) => 'date',
            $company['query1'][0]->subscribed_customers => 'subscribed_customers',
        ];

        if (isset($company['query2'])) {
            $template[$company['query2'][0]->disabled_subscribed_customers] = 'disabled_subscribed_customers';
        }

        if (isset($company['query3'])) {
            $i = 0;
            foreach ($company['query3'] as $tariff) {
                $template['tariffs_by_customers'][$i][$tariff->tariff_name] = 'tariff_name';
                $template['tariffs_by_customers'][$i][$tariff->customers_count] = 'customer';
                $i++;
            }
        }

        if (isset($company['query4'])) {
            $i = 0;
            foreach ($company['query4'] as $customer) {
                $template['customers_list'][$i][$customer->customer_name] = 'enabled_customer';
                $template['customers_list'][$i][$customer->tariff_name] = 'tariff';
                $i++;
            }
        }

        return $template;
    }

    /**
     * Prepare Json template.
     *
     * @param $company
     *
     * @return array
     */
    public function prepareJsonTemplate($company)
    {
        $template = [
            'company_name' => $company['query1'][0]->company_name,
            'date' => date('d-m-Y H:i:s', time()),
            'subscribed_customers' => $company['query1'][0]->subscribed_customers
        ];

        if (isset($company['query2'])) {
            $template['disabled_subscribed_customers'] = $company['query2'][0]->disabled_subscribed_customers;
        }

        if (isset($company['query3'])) {
            $i = 0;
            foreach ($company['query3'] as $tariff) {
                $template['tariffs_by_customers'][$i]['tariff_name'] = $tariff->tariff_name;
                $template['tariffs_by_customers'][$i]['customer'] = $tariff->customers_count;
                $i++;
            }
        }

        if (isset($company['query4'])) {
            $i = 0;
            foreach ($company['query4'] as $customer) {
                $template['customers_list'][$i]['enabled_customer'] = $customer->customer_name;
                $template['customers_list'][$i]['tariff'] = $customer->tariff_name;
                $i++;
            }
        }

        return $template;
    }

}
