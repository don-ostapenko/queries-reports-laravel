<?php

namespace App\Console\Commands;

use App\Services\ReportService;
use Illuminate\Console\Command;

class GenerateCliReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command that generate reports.';

    /**
     * @var \App\Services\ReportService
     */
    protected $reportService;

    /**
     * GenerateCliReports constructor.
     *
     * @param  \App\Services\ReportService  $reportService
     */
    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Exception
     */
    public function handle()
    {
        $this->reportService->makeReports();
        $this->info('Reports were successfully generated. Please, see "resources/reports" directory.');
    }
}
