## About application

This is a simple app, that include the queries to DB and artisan CLI command for generating reports based received data.

Steps of deployment

- Clone the project.
- Run `docker-compose up -d` in root project.
- Run `docker-compose run --rm php composer install` for install dependencies.
- Run `docker-compose run --rm php php artisan migrate --seed` for make migrations with seeding.
- Run `docker-compose run --rm php php artisan generate:reports` for generating reports.
